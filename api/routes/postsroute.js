const express = require('express');
const {getPosts,createPosts} = require('../controllers/postscontroller');
const router = express.Router();

router.get('/getPosts', getPosts);
router.post('/createPosts',createPosts);
module.exports = router;
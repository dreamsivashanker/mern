const mongoose = require("mongoose");
const crypto = require('crypto');

const userSchema = mongoose.Schema({
    name : {
         type : String,
         required : "Name is required",
         trim : true
    },
    email: {
        type: String,
        required: "Email is required",
        trim : true,
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
    },
    password: {
        type: String,
        required: "Password is required",
        trim: true
    },
    salt: String,
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date
    }

});

//userSchema.virtual("password")
//    .set((password) => {
//        this._password = password;
//        this.salt = uuidv1();
//        this.hashed_password = this.encryptpassword();
//    })
//    .get(() => {
//        return this._password 
//    })
//
//userSchema.methods = {
//    encryptpassword: function (password) {
//        if (!password) return "";
//        try {
//            crypto.createHmac('sha1', this.salt)
//                .update(password)
//                .digest('hex');
//        }
//        catch (err) {
//            return "";
//        }
//    }
//
//}

//userSchema.pre('save',async (next) =>{
//   const salt=await bcrypt.gensalt(10);
//   this.password = await bcrypt.hash(this.password,salt);
//});
module.exports = mongoose.model("users", userSchema);
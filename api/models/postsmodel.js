const mongoose = require('mongoose');
const posts = new mongoose.Schema({
    title : {
        type : String,
        required : "Title is required",
        minlength : 4,
        maxlength : 100
    },
    body:
    {
        type : String,
        required : "Body is required",
        minlength : 10,
        maxlength : 500
        
    }
});
module.exports = mongoose.model("posts",posts);


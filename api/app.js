const express=require('express');
const app=express();
const morgan=require('morgan');
const dotenv=require('dotenv');
const mongoose=require('mongoose');
const bodyparser = require('body-parser');

mongoose.connect("mongodb://localhost/nodeapi",{ useUnifiedTopology: true,useNewUrlParser: true}).then(()=> console.log("connected mongo"));
dotenv.config();

const postroute = require('./routes/postsroute');
const authroute = require('./routes/authroute');
app.use(morgan('dev'));
app.use(bodyparser.json());
app.use('/api',postroute);
app.use('/api',authroute);


const port=process.env.port||3000;
app.listen(port,() => { console.log("connected to port 3000")});
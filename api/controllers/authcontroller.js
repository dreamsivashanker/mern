const userSchema = require("../models/usermodel");

const bcrypt = require('bcryptjs');
exports.signUp =async (req,res)=>{
  
    //check if user exists
    const userexists = await userSchema.findOne({email : req.body.email});
    if(userexists)
    {
        res.status(403).json({error : " Email already taken"});
    }
    //req.body.password=bcrypt.hashSync(req.body.password, 10);
    const user=new userSchema(req.body);
    await user.save();
    res.status(200).json({user});
};

exports.signIn= async (req,res) =>{
    //req.body.password=bcrypt.hashSync(req.body.password, 10);
    //res.send(req.body.password); 
    const checkUser=await userSchema.findOne({"email" : req.body.email});
    
    if(!bcrypt.compareSync(req.body.password, checkUser.password)) {
            return res.status(400).send({ message: "The password is invalid" });
        }
    if(checkUser)
    {
       res.status(200).json(checkUser)  
    }
    else
    {
    res.send(403).json({err:"invalid credentials"});  
    }
};